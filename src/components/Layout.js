import React, { useState, useEffect, forwardRef } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
  Grid,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Drawer,
} from '@material-ui/core';
import { Menu } from '@material-ui/icons';
import PropTypes from 'prop-types';
import logo from '../assets/UP.svg';
import { useStyles } from '../styles/Layout';
import GonoMenu from './GonoMenu';

function Layout({ children, title }) {
  const renderLink = forwardRef((itemProps, ref) => (
    <RouterLink {...itemProps} ref={ref} />
  ));
  const classes = useStyles();
  const [openDrawer, setOpenDrawer] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);

  useEffect(() => {
    document.title = title;
  });

  function handleDrawerToggle() {
    setOpenDrawer(!openDrawer);
  }

  function handleListItemClick(event, index) {
    setSelectedIndex(index);
    setOpenDrawer(false);
  }

  return (
    <Grid container>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <Menu />
          </IconButton>
          <img src={logo} className={classes.logo} alt="logo" />
          <Typography
            component={renderLink}
            to="/"
            variant="h6"
            noWrap
            onClick={event => handleListItemClick(event, 0)}
            className={classes.title}
          >
            {title}
          </Typography>
          <GonoMenu
            selectedIndex={selectedIndex}
            handleListItemClick={handleListItemClick}
            renderLink={renderLink}
            classes={classes}
            horizontal
          />
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}>
        <Drawer
          variant="temporary"
          open={openDrawer}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <GonoMenu
            selectedIndex={selectedIndex}
            handleListItemClick={handleListItemClick}
            renderLink={renderLink}
            classes={classes}
          />
        </Drawer>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {children}
      </main>
    </Grid>
  );
}

Layout.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape),
  title: PropTypes.string.isRequired,
};

Layout.defaultProps = {
  children: () => {},
};

export default Layout;
