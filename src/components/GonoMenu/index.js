import React from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';
import { AddBox, ClearAll } from '@material-ui/icons';

function GonoMenu(props) {
  const {
    selectedIndex,
    handleListItemClick,
    renderLink,
    horizontal,
    classes,
  } = props;

  const items = [
    {
      label: 'Lista',
      link: '/',
    }, {
      label: 'Editar',
      link: '/edit-list',
    }];

  return (
    <List className={ horizontal ? classes.horizontal : ''}>
      {items.map((item, index) => (
        <ListItem
          button
          component={renderLink}
          to={item.link}
          key={item.label}
          selected={selectedIndex === index}
          disabled={selectedIndex === index}
          onClick={event => handleListItemClick(event, index)}
          className={classes.listItem}
        >
          <ListItemIcon className={classes.drawerIcon}>
            {index % 2 === 0 ? <ClearAll /> : <AddBox />}
          </ListItemIcon>
          <ListItemText primary={item.label} />
        </ListItem>
      ))}
    </List>
  );
}

export default GonoMenu;
