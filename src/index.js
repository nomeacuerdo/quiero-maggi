import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import {
  CssBaseline,
  MuiThemeProvider,
  createMuiTheme,
} from '@material-ui/core';
import { grey, blueGrey, red } from '@material-ui/core/colors';
import ViewCards from './pages/ViewCards';
import EditCards from './pages/EditCards';
import Layout from './components/Layout';
import { Provider } from './store';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: grey[800],
    },
    secondary: blueGrey,
  },
  status: {
    danger: red.A700,
  },
});

const title = [
  'Quiero carton sabroso',
  'QUE RICO EL BISIO',
  'Maggi zavrozo',
  'Lo que buscan las PSHAS',
];
const rand = Math.floor(Math.random() * Math.floor(title.length));

ReactDOM.render(
  <Provider>
    <Router>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Layout title={title[rand]}>
          <Route exact path="/" component={ViewCards} />
          <Route path="/edit-list" component={EditCards} />
        </Layout>
      </MuiThemeProvider>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
