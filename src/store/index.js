/* eslint-disable react/prop-types */
import React, { useReducer } from 'react';
// import ProfileReducer from '../store/ProfileReducer';
// import profileState from '../store/profileState';

const Store = React.createContext();

const CreateStore = (reducer, initialState) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return { state, dispatch };
};

const Provider = ({ children }) => {
  // const store = CreateStore(ProfileReducer, profileState);
  const store = CreateStore();
  return <Store.Provider value={store}>{children}</Store.Provider>;
};

export { Store, Provider };
